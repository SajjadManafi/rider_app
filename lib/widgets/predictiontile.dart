import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rider_app/datamodels/address.dart';
import 'package:rider_app/datamodels/prediction.dart';
import 'package:rider_app/widgets/ProgrssDialog.dart';

import '../brand_colors.dart';
import '../dataprovider/appdata.dart';
import '../globalVariable.dart';
import '../helpers/requesthelper.dart';

class PredictionTile extends StatelessWidget {
  final Prediction prediction;

  PredictionTile({required this.prediction});

  void getPlaceDetails(String placeID, context) async {

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => ProgressDialog(status: 'Please wait...'),
    );

    String url =
        "https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeID&key=$mapKey";
    var response = await RequestHelper.getRequest(url);

    Navigator.pop(context);

    if (response == 'failed') {
      return;
    }

    if (response['status'] == 'OK') {
        Address thisPlace = Address();
        thisPlace.placeId = placeID;
        thisPlace.placeName = response['result']['name'];
        thisPlace.latitude = response['result']['geometry']['location']['lat'];
        thisPlace.longitude = response['result']['geometry']['location']['lng'];



      Provider.of<AppData>(context, listen: false).updateDestinationAddress(thisPlace);
      print(thisPlace.placeName);

      Navigator.pop(context, 'getDirection');
    }
  }

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () {
        getPlaceDetails(prediction.placeId ?? '', context);
      },
      padding: EdgeInsets.all(0),
      child: Container(
        child: Column(
          children: [
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                const Icon(
                  Icons.location_on_outlined,
                  color: BrandColors.colorDimText,
                ),
                const SizedBox(
                  width: 12,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        prediction.mainText ?? '',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: const TextStyle(
                          fontSize: 16,
                        ),
                      ),
                      const SizedBox(
                        height: 2,
                      ),
                      Text(
                        prediction.secondaryText ?? '',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: const TextStyle(
                          fontSize: 16,
                          color: BrandColors.colorDimText,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 8,
            ),
          ],
        ),
      ),
    );
  }
}
