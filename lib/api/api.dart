import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

const SERVER_URL = 'http://localhost:8080';
final storage = FlutterSecureStorage();

Future<int> attemptLogin(String username, String password) async {
  var url = Uri.parse('http://192.168.1.105:8080/api/login');
  var res = await http.post(
    url,
    body: jsonEncode(<String, String>{
      'username': username,
      'password': password,
      'type':'rider',
    }),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );
  if (res.statusCode == 200) {
    final jsonResponse = json.decode(res.body);
    if (jsonResponse != null && jsonResponse['access_token'] != null && jsonResponse['user']['username'] != null) {
      await storage.write(key: 'token', value: jsonResponse['access_token']);
      await storage.write(key: 'username', value: jsonResponse['user']['username']);
      await storage.write(key: 'password', value: password);
      return 200;
    }
  }
  return 400;
}

Future<int> attemptSignUp(String username, String password, String full_name,
    String gender, String email) async {
  var url = Uri.parse('http://192.168.1.105:8080/api/users');

  var res = await http.post(
    url,
    body: jsonEncode(<String, String>{
      'username': username,
      'password': password,
      'full_name': full_name,
      'gender': gender,
      'email': email,
    }),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );

  return res.statusCode;
}
