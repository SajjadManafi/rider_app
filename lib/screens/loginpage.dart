// ignore_for_file: unnecessary_const, unnecessary_new

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:rider_app/screens/mainpage.dart';
import 'package:rider_app/screens/registerpage.dart';

import '../widgets/TaxiButton.dart';
import '../widgets/ProgrssDialog.dart';

import '../api/api.dart';

import '../brand_colors.dart';

class LoginPage extends StatefulWidget {
  static const String id = 'login';

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  // final Connectivity _connectivity = Connectivity();
  void showSnackBar(String message) {
    final snackBar = SnackBar(
      content: Text(
        message,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 15),
      ),
    );
    scaffoldKey.currentState?.showSnackBar(snackBar);
  }

  var usernameController = TextEditingController();

  var passwordController = TextEditingController();

  void login() async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => ProgressDialog(status: "Signing you in...",),
    );

    var res = await attemptLogin(usernameController.text, passwordController.text).catchError((err){
      Navigator.pop(context);
      showSnackBar(err);
    });

    if (res == 200) { 
      Navigator.pushNamedAndRemoveUntil(context, MainPage.id, (route) => false);
    } else {
      Navigator.pop(context);
      showSnackBar('SignIn failed');
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 70,
                ),
                const Image(
                  alignment: Alignment.center,
                  height: 100.0,
                  width: 100.0,
                  image: const AssetImage('images/logo.png'),
                ),
                const SizedBox(
                  height: 40,
                ),
                const Text(
                  'Sign in as Rider',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: 'Brand-Bold',
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      TextField(
                        controller: usernameController,
                        keyboardType: TextInputType.text,
                        decoration: const InputDecoration(
                          labelText: 'Username',
                          labelStyle: const TextStyle(
                            fontSize: 14.0,
                          ),
                          hintStyle: const TextStyle(
                            color: Colors.grey,
                            fontSize: 10.0,
                          ),
                        ),
                        style: const TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextField(
                        controller: passwordController,
                        obscureText: true,
                        decoration: const InputDecoration(
                          labelText: 'Password',
                          labelStyle: TextStyle(
                            fontSize: 14.0,
                          ),
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontSize: 10.0,
                          ),
                        ),
                        style: const TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      TaxiButton(
                        title: 'LOGIN',
                        onPressed: () {
                          // check network and inputs 
                          login();
                        },
                        color: BrandColors.colorGreen,
                      ),
                    ],
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, RegisterPage.id, (route) => false);
                  },
                  child: const Text('Don\'t have an account?, Sign up here'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
