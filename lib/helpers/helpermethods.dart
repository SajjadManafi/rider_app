import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rider_app/datamodels/address.dart';
import 'package:rider_app/datamodels/directiondetails.dart';
import 'package:rider_app/globalVariable.dart';
import 'package:rider_app/helpers/requesthelper.dart';
import '../dataprovider/appdata.dart';

class HelperMethods {
  static Future<String> findCoordinateAddress(
      Position position, context) async {
    String placeAddress = '';
    String url =
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&key=' +
            mapKey;

    var response = await RequestHelper.getRequest(url);

    if (response != 'failed') {
      placeAddress = response['results'][0]['formatted_address'];
      print("************************************************");
      print("****************8i do this fucking shit****************8");
      print("************************************************");
      Address pickupAddress = Address();

      pickupAddress.longitude = position.longitude;
      pickupAddress.latitude = position.latitude;
      pickupAddress.placeName = placeAddress;

      print("************************************************");
      print(pickupAddress.placeName);
      print("************************************************");

      Provider.of<AppData>(context, listen: false)
          .updatePickupAddress(pickupAddress);
    }

    return placeAddress;
  }

  static Future<DirectionDetails?> getDirectionDetails(LatLng startPosition, LatLng endPosition) async {
    String url =
        'https://maps.googleapis.com/maps/api/directions/json?origin=${startPosition.latitude},${startPosition.longitude}&destination=${endPosition.latitude},${endPosition.longitude}&mode=driving&key=$mapKey';

    var response = await RequestHelper.getRequest(url);

    if (response == 'failed') {
      return null;
    }

    
    var durationText = response['routes'][0]['legs'][0]['duration']['text'];
    var durationValue = response['routes'][0]['legs'][0]['duration']['value'];

    var distanceText = response['routes'][0]['legs'][0]['distance']['text'];
    var distanceValue = response['routes'][0]['legs'][0]['distance']['value'];

    var encodedPoints = response['routes'][0]['overview_polyline']['points'];

    DirectionDetails directionDetails = DirectionDetails(
      distanceText: distanceText,
      durationText: durationText,
      distanceValue: distanceValue,
      durationValue: durationValue,
      encodedPoints: encodedPoints,
    );

    return directionDetails;
  }

  static int estimateFare(DirectionDetails details) {
      double baseFare = 3.0;
      double distanceFare = (details.distanceValue / 1000) * 0.3;
      double timeFare = (details.durationValue / 60) * 0.2;

      double totalFare = baseFare + distanceFare + timeFare;

      return totalFare.truncate();
  }
}
