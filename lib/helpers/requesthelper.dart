import 'dart:convert';
import 'package:http/http.dart' as http;

class RequestHelper {
  static Future<dynamic> getRequest(String url) async {
    var reqUrl = Uri.parse(url);
    http.Response response = await http.get(reqUrl);

    try {
      if (response.statusCode == 200) {
        String data = response.body;
        var decodedData = json.decode(data);
        print(decodedData);
        return decodedData;
      } else {
        return 'failed';
      }
    } catch (e) {
      return 'failed';
    }
  }
}
