import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:rider_app/dataprovider/appdata.dart';
import './screens/mainpage.dart';
import './screens/loginpage.dart';
import './screens/registerpage.dart';
import 'package:provider/provider.dart';

void main(){
  runApp(MyApp());
} 

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => AppData(),
      child: MaterialApp(
        theme: ThemeData(
          fontFamily: 'Brand-Regular',
          primarySwatch: Colors.blue,
        ),
        initialRoute: MainPage.id,
        routes: {
          RegisterPage.id: (context) => RegisterPage(),
          LoginPage.id: (context) => LoginPage(),
          MainPage.id: (context) => MainPage(),
        },
      ),
    );
  }
}
